name = "ocio"
version = "1.10.0.z0"
uuid = '5f2447f3-ac4e-49ae-8690-eb9ca68e7475'
description = \
    """
    Open Color IO
    """

authors = ['Not supplied']

tools = [
    'ociobakelut',
    'ociocheck',
    'ocioconvert',
    'ociodisplay',
    'ociolutimage',
]


variants = [
    ["platform-linux", "boost-1.61"],
]

requires = [
    'python-2.7.13',
    'ilmbase-2.2',
    'openexr-2.2',
    'hdf5-1.8',
    'zlib-1.2.11',
    'field3d-1.4.1',
    'szip-2.1',
    'libpng-1.6.30',
    'libraw-0.18.2',
    'openjpeg-2.3',
    'libtiff-4.0.7',
    'giflib-5.1.4',
    'boost',
    'opencv-3.3.1',
    'GLEW-1.13.0',
    'libwebp-0.6.0',
    'lcms2-2.8',
    'jasper-2.0.13',
    'fontconfig-2.11.95',
    #'pybind11-2.2+<3',
]

build_requires = ['truelight-4',]
private_build_requires = ['freeglut-3', 'qt_devel', 'patchelf-0', 'ptex-2', 'aces_container-1', 'nasm-2.13', 'gcc-4', 'cmake-3.4+<4']

def commands():
    #env.LD_LIBRARY_PATH.prepend("{root}/lib")
    env.PYTHONPATH.append("{root}/lib/python2.7/site-packages")

    if building:
        env.CMAKE_MODULE_PATH.append("{root}/cmake")

    if building:
        env.CMAKE_INCLUDE_PATH.prepend('{root}/include')
        env.CMAKE_LIBRARY_PATH.prepend('{root}/lib')
        env.CFLAGS.set('-I{root}/include $CFLAGS')
        env.CPPFLAGS.set('-I{root}/include $CPPFLAGS')
        env.CXXFLAGS.set('-I{root}/include $CXXFLAGS')
        env.LDFLAGS.set('-L{root}/lib -Wl,-rpath,{root}/lib $LDFLAGS')


variants = [
    #['platform-linux', 'arch-x86_64', 'os-CentOS-7', 'python-2.7.13', 'boost-1.55.0'],
    #['platform-linux', 'arch-x86_64', 'os-CentOS-7', 'python-2.7.13', 'boost-1.55.0', 'qt-5.6.1'],
    #['platform-linux', 'arch-x86_64', 'os-CentOS-7', 'python-2.7.13', 'boost-1.61.0'],
    ['platform-linux', 'arch-x86_64', 'os-CentOS-7', 'python-2.7.13', 'boost-1.61.0', 'qt-5.6.1'],

]

# def nuke_root(version):
#     return """{nuke_base}{version}{nuke_suffix}""".format(
#         nuke_base=nuke_base,
#         version=version,
#         nuke_suffix=nuke_suffix,
#     )
# nuke_version = "11.2.3.release"
# nuke_base = "/sw/packages/external/nuke/"
# nuke_suffix = "/platform-linux/arch-x86_64/ext/"
# nuke_enabled="-DNUKE_ENABLED=ON"
# nuke_root = "-DNuke_ROOT="+nuke_root(nuke_version)
# nuke_api = "-DNUKE_API={}".format(".".join(nuke_version.split('.')[0:2]))


with scope('config') as config:
    config.release_packages_path = '${METHOD_REZ_EXTERNAL_PACKAGES}'



