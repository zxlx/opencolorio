/*
    Made by Autodesk Inc. under the terms of the OpenColorIO BSD 3 Clause License
*/


#ifndef INCLUDED_OCIO_RANGE_H
#define INCLUDED_OCIO_RANGE_H

#include <OpenColorIO/OpenColorIO.h>

#include "Op.h"

#include <vector>

OCIO_NAMESPACE_ENTER
{
    
    // Create a range
    void CreateRangeOp(OpRcPtrVec & ops,
                       float minInValue, float maxInValue, float minOutValue, float maxOutValue,
                       TransformDirection direction);

    // Create an identity range
    void CreateIdentityRangeOp(OpRcPtrVec & ops, TransformDirection direction);

    namespace RangeUtils
    {
        // Get value which meaning that no limit applies
        float GetNoLimitValue();

        // Is a no limit value ?
        bool IsNoLimitValue(float value);

        // Check if two values (from range limits) are equal
        bool AreEqualValues(float left, float right, float absoluteError);
    }
}
OCIO_NAMESPACE_EXIT

#endif
