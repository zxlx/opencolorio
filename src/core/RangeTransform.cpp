/*
    Made by Autodesk Inc. under the terms of the OpenColorIO BSD 3 Clause License
*/


#include <cstring>

#include <OpenColorIO/OpenColorIO.h>

#include "OpBuilders.h"
#include "RangeOps.h"
#include "MathUtils.h"


OCIO_NAMESPACE_ENTER
{
    RangeTransformRcPtr RangeTransform::Create()
    {
        return RangeTransformRcPtr(new RangeTransform(), &deleter);
    }
    
    void RangeTransform::deleter(RangeTransform* t)
    {
        delete t;
    }
    
    class RangeTransform::Impl
    {
    public:
        Impl()
            :   m_direction(TRANSFORM_DIR_FORWARD)
            ,   m_minInValue(RangeUtils::GetNoLimitValue())
            ,   m_maxInValue(RangeUtils::GetNoLimitValue())
            ,   m_minOutValue(RangeUtils::GetNoLimitValue())
            ,   m_maxOutValue(RangeUtils::GetNoLimitValue())
        {
        }

        ~Impl()
        {
        }

        Impl& operator= (const Impl & rhs)
        {
            m_direction    = rhs.m_direction;
            m_minInValue   = rhs.m_minInValue;
            m_maxInValue   = rhs.m_maxInValue;
            m_minOutValue  = rhs.m_minOutValue;
            m_maxOutValue  = rhs.m_maxOutValue;
            return *this;
        }

        TransformDirection m_direction;
        float m_minInValue;   // Minimum value acceptable for the input
        float m_maxInValue;   // Maximun value acceptable for the input
        float m_minOutValue;  // Minimum value acceptable for the output
        float m_maxOutValue;  // Maximum value acceptable for the output
    };

    ///////////////////////////////////////////////////////////////////////////
    
    
    
    RangeTransform::RangeTransform()
        : m_impl(new RangeTransform::Impl)
    {
    }
    
    TransformRcPtr RangeTransform::createEditableCopy() const
    {
        RangeTransformRcPtr transform = RangeTransform::Create();
        *(transform->m_impl) = *m_impl;
        return transform;
    }
    
    RangeTransform::~RangeTransform()
    {
        delete m_impl;
        m_impl = NULL;
    }
    
    RangeTransform& RangeTransform::operator= (const RangeTransform & rhs)
    {
        *m_impl = *rhs.m_impl;
        return *this;
    }
    
    TransformDirection RangeTransform::getDirection() const
    {
        return getImpl()->m_direction;
    }
    
    void RangeTransform::setDirection(TransformDirection dir)
    {
        getImpl()->m_direction = dir;
    }
    
    bool RangeTransform::equals(const RangeTransform & other) const
    {
        static const float abserror = 1e-9f;
        
        return
               RangeUtils::AreEqualValues(getImpl()->m_minInValue, other.getImpl()->m_minInValue, abserror)
            && RangeUtils::AreEqualValues(getImpl()->m_maxInValue, other.getImpl()->m_maxInValue, abserror)
            && RangeUtils::AreEqualValues(getImpl()->m_minOutValue, other.getImpl()->m_minOutValue, abserror)
            && RangeUtils::AreEqualValues(getImpl()->m_maxOutValue, other.getImpl()->m_maxOutValue, abserror);
    }

    void RangeTransform::setMinInValue(float val)
    {
        getImpl()->m_minInValue = val;
    }

    float RangeTransform::getMinInValue() const
    {
        return getImpl()->m_minInValue;
    }

    bool RangeTransform::hasMinInValue() const
    {
        return !RangeUtils::IsNoLimitValue(getImpl()->m_minInValue);
    }

    void RangeTransform::unsetMinInValue()
    {
        getImpl()->m_minInValue = RangeUtils::GetNoLimitValue();
    }


    void RangeTransform::setMaxInValue(float val)
    {
        getImpl()->m_maxInValue = val;
    }

    float RangeTransform::getMaxInValue() const
    {
        return getImpl()->m_maxInValue;
    }

    bool RangeTransform::hasMaxInValue() const
    {
        return !RangeUtils::IsNoLimitValue(getImpl()->m_maxInValue);
    }

    void RangeTransform::unsetMaxInValue()
    {
        getImpl()->m_maxInValue = RangeUtils::GetNoLimitValue();
    }


    void RangeTransform::setMinOutValue(float val)
    {
        getImpl()->m_minOutValue = val;
    }

    float RangeTransform::getMinOutValue() const
    {
        return getImpl()->m_minOutValue;
    }

    bool RangeTransform::hasMinOutValue() const
    {
        return !RangeUtils::IsNoLimitValue(getImpl()->m_minOutValue);
    }

    void RangeTransform::unsetMinOutValue()
    {
        getImpl()->m_minOutValue = RangeUtils::GetNoLimitValue();
    }


    void RangeTransform::setMaxOutValue(float val)
    {
        getImpl()->m_maxOutValue = val;
    }

    float RangeTransform::getMaxOutValue() const
    {
        return getImpl()->m_maxOutValue;
    }

    bool RangeTransform::hasMaxOutValue() const
    {
        return !RangeUtils::IsNoLimitValue(getImpl()->m_maxOutValue);
    }

    void RangeTransform::unsetMaxOutValue()
    {
        getImpl()->m_maxOutValue = RangeUtils::GetNoLimitValue();
    }


    std::ostream& operator<< (std::ostream& os, const RangeTransform& t)
    {
        os << "<RangeTransform ";
        os << "direction=" << TransformDirectionToString(t.getDirection());
        if(t.hasMinInValue())  os << ", " << "minInValue="  << t.getMinInValue();
        if(t.hasMaxInValue())  os << ", " << "maxInValue="  << t.getMaxInValue();
        if(t.hasMinOutValue()) os << ", " << "minOutValue=" << t.getMinOutValue();
        if(t.hasMaxOutValue()) os << ", " << "maxOutValue=" << t.getMaxOutValue();
        os << ">";
        return os;
    }



    ///////////////////////////////////////////////////////////////////////////

    void BuildRangeOps(OpRcPtrVec & ops,
                       const Config& /*config*/,
                       const RangeTransform & transform,
                       TransformDirection dir)
    {
        const TransformDirection combinedDir 
            = CombineTransformDirections(dir, transform.getDirection());

        CreateRangeOp(ops, transform.getMinInValue(), transform.getMaxInValue(), 
            transform.getMinOutValue(), transform.getMaxOutValue(), combinedDir);
    }
    
}
OCIO_NAMESPACE_EXIT


////////////////////////////////////////////////////////////////////////////////


#ifdef OCIO_UNIT_TEST

namespace OCIO = OCIO_NAMESPACE;
#include "UnitTest.h"

#include <sstream>


OCIO_NAMESPACE_USING


OIIO_ADD_TEST(RangeTransform, Basic)
{
    OCIO::RangeTransformRcPtr range = OCIO::RangeTransform::Create();
    OIIO_CHECK_EQUAL(range->getDirection(), OCIO::TRANSFORM_DIR_FORWARD);
    OIIO_CHECK_ASSERT(RangeUtils::IsNoLimitValue(range->getMinInValue()));
    OIIO_CHECK_ASSERT(!range->hasMinInValue());
    OIIO_CHECK_ASSERT(RangeUtils::IsNoLimitValue(range->getMaxInValue()));
    OIIO_CHECK_ASSERT(!range->hasMaxInValue());
    OIIO_CHECK_ASSERT(RangeUtils::IsNoLimitValue(range->getMinOutValue()));
    OIIO_CHECK_ASSERT(!range->hasMinOutValue());
    OIIO_CHECK_ASSERT(RangeUtils::IsNoLimitValue(range->getMaxOutValue()));
    OIIO_CHECK_ASSERT(!range->hasMaxOutValue());

    range->setDirection(OCIO::TRANSFORM_DIR_INVERSE);
    OIIO_CHECK_EQUAL(range->getDirection(), OCIO::TRANSFORM_DIR_INVERSE);

    range->setMinInValue(-0.5f);
    OIIO_CHECK_EQUAL(range->getMinInValue(), -0.5f);
    OIIO_CHECK_ASSERT(range->hasMinInValue());

    OCIO::RangeTransformRcPtr range2 = OCIO::RangeTransform::Create();
    range2->setDirection(OCIO::TRANSFORM_DIR_INVERSE);
    range2->setMinInValue(-0.5f);
    OIIO_CHECK_ASSERT(range2->equals(*range));

    range2->setDirection(OCIO::TRANSFORM_DIR_FORWARD);
    range2->setMinInValue(RangeUtils::GetNoLimitValue());
    range2->setMaxInValue(-0.5f);
    range2->setMinOutValue(1.5f);
    range2->setMaxOutValue(4.5f);

    OIIO_CHECK_ASSERT(RangeUtils::IsNoLimitValue(range2->getMinInValue()));
    OIIO_CHECK_EQUAL(range2->getMaxInValue(), -0.5f);
    OIIO_CHECK_EQUAL(range2->getMinOutValue(), 1.5f);
    OIIO_CHECK_EQUAL(range2->getMaxOutValue(), 4.5f);

    range2->setMinInValue(-1.5f);
    OIIO_CHECK_EQUAL(range2->getMinInValue(), -1.5f);
    OIIO_CHECK_EQUAL(range2->getMaxInValue(), -0.5f);
    OIIO_CHECK_EQUAL(range2->getMinOutValue(), 1.5f);
    OIIO_CHECK_EQUAL(range2->getMaxOutValue(), 4.5f);

    OIIO_CHECK_ASSERT(range2->hasMinInValue());
    OIIO_CHECK_ASSERT(range2->hasMaxInValue());
    OIIO_CHECK_ASSERT(range2->hasMinOutValue());
    OIIO_CHECK_ASSERT(range2->hasMaxOutValue());

    range2->unsetMinInValue();
    OIIO_CHECK_ASSERT(!range2->hasMinInValue());
    OIIO_CHECK_ASSERT(range2->hasMaxInValue());
    OIIO_CHECK_ASSERT(range2->hasMinOutValue());
    OIIO_CHECK_ASSERT(range2->hasMaxOutValue());

    range2->unsetMaxInValue();
    OIIO_CHECK_ASSERT(!range2->hasMinInValue());
    OIIO_CHECK_ASSERT(!range2->hasMaxInValue());
    OIIO_CHECK_ASSERT(range2->hasMinOutValue());
    OIIO_CHECK_ASSERT(range2->hasMaxOutValue());

    range2->unsetMinOutValue();
    OIIO_CHECK_ASSERT(!range2->hasMinInValue());
    OIIO_CHECK_ASSERT(!range2->hasMaxInValue());
    OIIO_CHECK_ASSERT(!range2->hasMinOutValue());
    OIIO_CHECK_ASSERT(range2->hasMaxOutValue());

    range2->unsetMaxOutValue();
    OIIO_CHECK_ASSERT(!range2->hasMinInValue());
    OIIO_CHECK_ASSERT(!range2->hasMaxInValue());
    OIIO_CHECK_ASSERT(!range2->hasMinOutValue());
    OIIO_CHECK_ASSERT(!range2->hasMaxOutValue());
}

#endif
