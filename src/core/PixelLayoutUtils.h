/*
Copyright (c) 2003-2010 Sony Pictures Imageworks Inc., et al.
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of Sony Pictures Imageworks nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef INCLUDED_OCIO_PIXEL_LAYOUT_UTILS_H
#define INCLUDED_OCIO_PIXEL_LAYOUT_UTILS_H

#include <OpenColorIO/OpenColorIO.h>

#include "Op.h"

#include <vector>
#include <algorithm>


OCIO_NAMESPACE_ENTER
{
    #define GET_BIT_DEPTH(in) BitDepth(in & 0x00FF)

    #define GET_PIXEL_LAYOUT(in) PixelLayout(in & 0xFF00)


    // The default template is empty to have a compilation error
    //   if an unsupported bit depth is used
    template<BitDepth>
    struct BitDepthInfo {};

    template<>
    struct BitDepthInfo<BIT_DEPTH_UINT8>
    {
        typedef uint8_t Type;
        static const unsigned max = 255;
        static const unsigned min = 0;

        static Type getValue(float val) 
        { 
            return (Type)std::min(std::max(val+0.5f, (float)min), (float)max); 
        }
    };

    template<>
    struct BitDepthInfo<BIT_DEPTH_F32>
    {
        typedef float Type;
        static const unsigned max = 1;
        static const unsigned min = 0;

        static Type getValue(float val) { return val; }
    };

    // Get max values for a specific bit depth.
    // 
    float GetBitDepthMax(BitDepth in);

    // Get the range of values for a specific bit depth.
    // 
    //  The BIT_DEPTH_F32 range is [0.0f, 1.0f] so the range is 1.0f
    //  (i.e. BitDepthInfo<BIT_DEPTH_F32>::max
    //          - BitDepthInfo<BIT_DEPTH_F32>::min )
    //
    float GetBitDepthRange(BitDepth in);

    // Get the scale factor applicable to input values to scale from
    //  the input bit depth to the output bit depth.
    //  
    //  The scale factor from BIT_DEPTH_F32 to BIT_DEPTH_UINT8 
    //  is to multiply the values by 255.0f
    //  (i.e. GetBitDepthRange(BIT_DEPTH_UINT8) /
    //           GetBitDepthRange(BIT_DEPTH_UINT8>  )
    //  
    float ComputeScaleFactor(BitDepth in, BitDepth out);

    // Adjust the op list to the expected input and output bit depths
    // 
    // Add a matrix op (i.e. a diagonal matrix with the scale factor):
    //  1. at the first poisition in the list to scale input values 
    //      to the bit depth of the first op
    //  2. at the last position in to op list to scale output values 
    //      from the last op to the expected output bit depth
    //  
    void AdjustToBitDepths(OpRcPtrVec & cpuOps, BitDepth in, BitDepth out);


    // Adjust the op list to the requested input and output pixel layouts
    //
    //  Add an op (i.e. a matrix op):
    //  1. at the first poisition in the list to convert from the input pixel lauyout 
    //      to RGBA by adding a BGRA inverse op
    //  2. at the last position in the op list to convert from RGBA to the output pixel layout
    //      by adding a BGRA forward op 
    //
    void AdjustToPixelLayouts(OpRcPtrVec & cpuOps, PixelFormat in, PixelFormat out);



#define BIT_DEPTH_APPLY(func, ...)\
    switch(srcBitDepth)\
    {\
        case BIT_DEPTH_F32:\
        {\
            switch(dstBitDepth)\
            {\
                case BIT_DEPTH_F32:\
                    func<BIT_DEPTH_F32, BIT_DEPTH_F32>(srcPixel, dstPixel, numPixels, ##__VA_ARGS__);\
                    break;\
                case BIT_DEPTH_UINT8:\
                    func<BIT_DEPTH_F32, BIT_DEPTH_UINT8>(srcPixel, dstPixel, numPixels, ##__VA_ARGS__);\
                    break;\
                case BIT_DEPTH_UNKNOWN:\
                case BIT_DEPTH_UINT10:\
                case BIT_DEPTH_UINT12:\
                case BIT_DEPTH_UINT14:\
                case BIT_DEPTH_UINT16:\
                case BIT_DEPTH_UINT32:\
                case BIT_DEPTH_F16:\
                    std::string err("Unsupported bit depth: ");\
                    err += std::string(BitDepthToString(dstBitDepth));\
                    throw Exception(err.c_str());\
                    break;\
            }\
            break;\
        }\
        case BIT_DEPTH_UINT8:\
        {\
            switch(dstBitDepth)\
            {\
                case BIT_DEPTH_F32:\
                    func<BIT_DEPTH_UINT8, BIT_DEPTH_F32>(srcPixel, dstPixel, numPixels, ##__VA_ARGS__);\
                    break;\
                case BIT_DEPTH_UINT8:\
                    func<BIT_DEPTH_UINT8, BIT_DEPTH_UINT8>(srcPixel, dstPixel, numPixels, ##__VA_ARGS__);\
                    break;\
                case BIT_DEPTH_UNKNOWN:\
                case BIT_DEPTH_UINT10:\
                case BIT_DEPTH_UINT12:\
                case BIT_DEPTH_UINT14:\
                case BIT_DEPTH_UINT16:\
                case BIT_DEPTH_UINT32:\
                case BIT_DEPTH_F16:\
                    std::string err("Unsupported bit depth: ");\
                    err += std::string(BitDepthToString(dstBitDepth));\
                    throw Exception(err.c_str());\
                    break;\
            }\
            break;\
        }\
        case BIT_DEPTH_UNKNOWN:\
        case BIT_DEPTH_UINT10:\
        case BIT_DEPTH_UINT12:\
        case BIT_DEPTH_UINT14:\
        case BIT_DEPTH_UINT16:\
        case BIT_DEPTH_UINT32:\
        case BIT_DEPTH_F16:\
            std::string err("Unsupported bit depth: ");\
            err += std::string(BitDepthToString(srcBitDepth));\
            throw Exception(err.c_str());\
            break;\
    }\

}
OCIO_NAMESPACE_EXIT

#endif // INCLUDED_OCIO_PIXEL_LAYOUT_UTILS_H
