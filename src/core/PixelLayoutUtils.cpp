/*
Copyright (c) 2003-2010 Sony Pictures Imageworks Inc., et al.
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of Sony Pictures Imageworks nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <OpenColorIO/OpenColorIO.h>

#include "PixelLayoutUtils.h"

#include "Op.h"
#include "MatrixOps.h"


OCIO_NAMESPACE_ENTER
{
    void CreateBGRAOp(OpRcPtrVec & ops, TransformDirection /*direction*/,
                      BitDepth in, BitDepth out)
    {
        static const float offsets[4]  = { 0.0f, 0.0f, 0.0f, 0.0f };

        static const float  matrix[16] = { 0.0f, 0.0f, 1.0f, 0.0f,
                                           0.0f, 1.0f, 0.0f, 0.0f,
                                           1.0f, 0.0f, 0.0f, 0.0f,
                                           0.0f, 0.0f, 0.0f, 1.0f };

        CreateMatrixOffsetOp(ops, &matrix[0], &offsets[0], TRANSFORM_DIR_FORWARD);
        ops[0]->setInputBitDepth(in);
        ops[0]->setOutputBitDepth(out);
    }

    void AdjustToPixelLayouts(OpRcPtrVec & cpuOps, PixelFormat in, PixelFormat out)
    {
        // Conversion to RGBA
        switch(GET_PIXEL_LAYOUT(in))
        {
            case PIXEL_LAYOUT_RGBA:
                break;

            case PIXEL_LAYOUT_BGRA:
            {
                OpRcPtrVec ops;
                CreateBGRAOp(ops, TRANSFORM_DIR_INVERSE, 
                             GET_BIT_DEPTH(in), GET_BIT_DEPTH(out));
                cpuOps.insert(cpuOps.begin(), ops[0]);
                break;
            }

            default:
                throw("Unknow pixel layout");
        }

        // Conversion from RGBA
        switch(GET_PIXEL_LAYOUT(out))
        {
            case PIXEL_LAYOUT_RGBA:
                break;

            case PIXEL_LAYOUT_BGRA:
            {
                OpRcPtrVec ops;
                CreateBGRAOp(ops, TRANSFORM_DIR_FORWARD, 
                             GET_BIT_DEPTH(in), GET_BIT_DEPTH(out));
                cpuOps.insert(cpuOps.end(), ops[0]);
                break;
            }

            default:
                throw("Unknow pixel layout");
        }

    }

    float GetBitDepthMax(BitDepth in)
    {
        switch(in)
        {
            case BIT_DEPTH_UINT8:
                return (float)BitDepthInfo<BIT_DEPTH_UINT8>::max;
                break;
            case BIT_DEPTH_F32:
                return (float)BitDepthInfo<BIT_DEPTH_F32>::max;
                break;

            case BIT_DEPTH_UNKNOWN:
            case BIT_DEPTH_UINT10:
            case BIT_DEPTH_UINT12:
            case BIT_DEPTH_UINT14:
            case BIT_DEPTH_UINT16:
            case BIT_DEPTH_UINT32:
            case BIT_DEPTH_F16:
                break;
        }

        std::string err("Unsupported bit depth: ");
        err += std::string(BitDepthToString(in));
        throw Exception(err.c_str());

        return 1.0f;
    }

    float GetBitDepthRange(BitDepth in)
    {
        switch(in)
        {
            case BIT_DEPTH_UINT8:
                return (float)BitDepthInfo<BIT_DEPTH_UINT8>::max
                            - (float)BitDepthInfo<BIT_DEPTH_UINT8>::min;
                break;
            case BIT_DEPTH_F32:
                return (float)BitDepthInfo<BIT_DEPTH_F32>::max
                            - (float)BitDepthInfo<BIT_DEPTH_F32>::min;
                break;

            case BIT_DEPTH_UNKNOWN:
            case BIT_DEPTH_UINT10:
            case BIT_DEPTH_UINT12:
            case BIT_DEPTH_UINT14:
            case BIT_DEPTH_UINT16:
            case BIT_DEPTH_UINT32:
            case BIT_DEPTH_F16:
                break;
        }

        std::string err("Unsupported bit depth: ");
        err += std::string(BitDepthToString(in));
        throw Exception(err.c_str());

        return 1.0f;
    }

    float ComputeScaleFactor(BitDepth in, BitDepth out)
    {
        return GetBitDepthRange(out) / GetBitDepthRange(in);
    }

    void AdjustToBitDepths(OpRcPtrVec & cpuOps, BitDepth in, BitDepth out)
    {
        // Apply an input scale from Apply bit depth to the first op bit depth
        if(in!=cpuOps[0]->getInputBitDepth())
        {
            cpuOps[0]->setInputBitDepth(in);
        }

        // Apply an output scale from last op bit depth to the Apply bit depth
        const OpRcPtrVec::size_type nOps = cpuOps.size();
        if(out!=cpuOps[nOps-1]->getOutputBitDepth())
        {
            cpuOps[nOps-1]->setOutputBitDepth(out);
        }
    }

}
OCIO_NAMESPACE_EXIT



///////////////////////////////////////////////////////////////////////////////

#ifdef OCIO_UNIT_TEST

OCIO_NAMESPACE_USING

#include "UnitTest.h"

OIIO_ADD_TEST(PixelLayoutOps, BGRAOp)
{
    OpRcPtrVec ops;

    // RGBA --> BGRA
    CreateBGRAOp(ops, TRANSFORM_DIR_FORWARD, BIT_DEPTH_F32, BIT_DEPTH_F32);
    // BGRA --> RGBA
    CreateBGRAOp(ops, TRANSFORM_DIR_INVERSE, BIT_DEPTH_F32, BIT_DEPTH_F32);

    OIIO_CHECK_EQUAL(ops.size(), 2);
    
    for(unsigned int i=0; i<ops.size(); ++i)
    {
        OIIO_CHECK_NO_THROW(ops[i]->finalize());
    }
    
    const unsigned NB_PIXELS = 4;

    const float source[NB_PIXELS*4] = {  -0.500f, 0.50f, 0.590108f, 0.0f,
                                         -0.250f, 0.75f, 0.505070f, 0.5f,
                                          0.001f, 1.00f, 0.500575f, 0.5f,
                                          0.500f, 1.25f, 0.599415f, 1.0f   };
    
    float result[NB_PIXELS*4];
    ops[0]->apply(BIT_DEPTH_F32, source, BIT_DEPTH_F32, result, NB_PIXELS);

    for(unsigned int i=0; i<NB_PIXELS; ++i)
    {
        OIIO_CHECK_EQUAL(source[4*i+0], result[4*i+2]);
        OIIO_CHECK_EQUAL(source[4*i+1], result[4*i+1]);
        OIIO_CHECK_EQUAL(source[4*i+2], result[4*i+0]);
        OIIO_CHECK_EQUAL(source[4*i+3], result[4*i+3]);
    }

    float res_reverse[NB_PIXELS*4];
    ops[1]->apply(BIT_DEPTH_F32, result, BIT_DEPTH_F32, res_reverse, NB_PIXELS);
    ops[1]->apply(BIT_DEPTH_F32, result, BIT_DEPTH_F32, result, NB_PIXELS);
    for(unsigned int i=0; i<(NB_PIXELS*4); ++i)
    {
        OIIO_CHECK_EQUAL(source[i], result[i]);
        OIIO_CHECK_EQUAL(source[i], res_reverse[i]);
    }
}

#endif // OCIO_UNIT_TEST
