/*
    Made by Autodesk Inc. under the terms of the OpenColorIO BSD 3 Clause License
*/


#include <OpenColorIO/OpenColorIO.h>
OCIO_NAMESPACE_ENTER
{

    Region::Region(unsigned x, unsigned y,
                   unsigned width, unsigned height,
                   unsigned imageWidth, unsigned imageHeight)
        :   m_x(x)
        ,   m_y(y)
        ,   m_width(width)
        ,   m_height(height)
        ,   m_imageWidth(imageWidth)
        ,   m_imageHeight(imageHeight)
    {
        if( (m_x+m_width) > m_imageWidth || (m_y+m_height) > m_imageHeight )
        {
            throw Exception("invalid region of interest");
        }
    }

    Region::Region(unsigned width, unsigned height)
        :   m_x(0)
        ,   m_y(0)
        ,   m_width(width)
        ,   m_height(height)
        ,   m_imageWidth(width)
        ,   m_imageHeight(height)
    {       
    }

    unsigned Region::getX() const
    {
        return m_x;        
    }

    unsigned Region::getY() const
    {
        return m_y;
    }

    unsigned Region::getWidth() const
    {
        return m_width;
    }

    unsigned Region::getHeight() const
    {
        return m_height;
    }

    unsigned Region::getImageWidth() const
    {
        return m_imageWidth;
    }

    unsigned Region::getImageHeight() const
    {
        return m_imageHeight;
    }

}
OCIO_NAMESPACE_EXIT


///////////////////////////////////////////////////////////////////////////////

#ifdef OCIO_UNIT_TEST

namespace OCIO = OCIO_NAMESPACE;
#include "UnitTest.h"


OIIO_ADD_TEST(Region, Basic)
{
    OCIO::Region r(0, 1, 10, 20, 100, 200);

    OIIO_CHECK_EQUAL(r.getX(), 0);
    OIIO_CHECK_EQUAL(r.getY(), 1);
    OIIO_CHECK_EQUAL(r.getWidth(), 10);
    OIIO_CHECK_EQUAL(r.getHeight(), 20);
    OIIO_CHECK_EQUAL(r.getImageWidth(), 100);
    OIIO_CHECK_EQUAL(r.getImageHeight(), 200);
}

OIIO_ADD_TEST(Region, Image)
{
    OCIO::Region r(100, 200);

    OIIO_CHECK_EQUAL(r.getX(), 0);
    OIIO_CHECK_EQUAL(r.getY(), 0);
    OIIO_CHECK_EQUAL(r.getWidth(), 100);
    OIIO_CHECK_EQUAL(r.getHeight(), 200);
    OIIO_CHECK_EQUAL(r.getImageWidth(), 100);
    OIIO_CHECK_EQUAL(r.getImageHeight(), 200);
}


#endif // OCIO_UNIT_TEST
