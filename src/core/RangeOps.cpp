/*
    Made by Autodesk Inc. under the terms of the OpenColorIO BSD 3 Clause License
*/


#include <OpenColorIO/OpenColorIO.h>

#include "GpuShaderUtils.h"
#include "HashUtils.h"
#include "RangeOps.h"
#include "MathUtils.h"
#include "SSE.h"

#include <cstring>
#include <sstream>


#if defined __APPLE__
    #define F_ISNAN(a) __inline_isnanf(a)
#elif defined(_WIN32) || defined(_WIN64) || defined(_WINDOWS) || defined(_MSC_VER)
    #define F_ISNAN(a) (_isnan(a)!=0)
#else
    #define F_ISNAN(a) __isnanf(a)
#endif


OCIO_NAMESPACE_ENTER
{
    namespace
    {
        class RangeOp : public Op
        {
        public:
            RangeOp(float minInValue, float maxInValue, 
                float minOutValue, float maxOutValue, TransformDirection direction);

            RangeOp(TransformDirection direction);

            virtual ~RangeOp();
            
            virtual OpRcPtr clone() const;
            
            virtual std::string getInfo() const;
            virtual std::string getCacheID() const;
            
            virtual bool isNoOp() const;
            virtual bool isSameType(const OpRcPtr & op) const;
            virtual bool isInverse(const OpRcPtr & op) const;

            virtual bool canCombineWith(const OpRcPtr & op) const;
            virtual void combineWith(OpRcPtrVec & ops, const OpRcPtr & secondOp) const;
            
            virtual bool hasChannelCrosstalk() const;
            virtual void finalize();
            virtual void apply(float * rgbaBuffer, long numPixels) const;
            
            virtual bool supportsGpuShader() const;
            virtual void writeGpuShader(std::ostream & shader,
                                        const std::string & pixelName,
                                        const GpuShaderDesc & shaderDesc) const;

            // Accessor methods to have the range limits
            inline float getMinInValue()  const { return m_minInValue;   }
            inline float getMaxInValue()  const { return m_maxInValue;   }
            inline float getMinOutValue() const { return m_minOutValue;  }
            inline float getMaxOutValue() const { return m_maxOutValue;  }

        protected:
            // Method to compute the scale and offset implied by the minimum
            //   and maximum range limits
            static void FillScaleOffset(
                float minInpValue, float maxInValue, float minOutValue, float maxOutValue, 
                float & scale, float & offset);

            // Is the range adding some scaling?
            bool scale() const;

            // Validate the range values
            void validate();
        
        private:
            float m_minInValue;   // The minimum acceptable value for the input
            float m_maxInValue;   // The maximum acceptable value for the input
            float m_minOutValue;  // The minimum acceptable value for the output
            float m_maxOutValue;  // The maximum acceptable value for the output

            // The range direction
            TransformDirection m_direction;

            // Computed scale and offset based on min & max values
            float m_scale;
            float m_offset;
            
            // The computed cache identifier
            std::string m_cacheID;
        };


        typedef OCIO_SHARED_PTR<RangeOp> RangeOpRcPtr;


        RangeOp::RangeOp(
            float minInValue, float maxInValue, float minOutValue, float maxOutValue, 
            TransformDirection direction)
            :   Op()
            ,   m_minInValue(minInValue)
            ,   m_maxInValue(maxInValue)
            ,   m_minOutValue(minOutValue)
            ,   m_maxOutValue(maxOutValue)
            ,   m_direction(direction)
            ,   m_scale(1.0f)
            ,   m_offset(0.0f)
        {
            if(m_direction == TRANSFORM_DIR_UNKNOWN)
            {
                throw Exception("Cannot apply RangeOp op, unspecified transform direction.");
            }

            validate();

            if(direction == TRANSFORM_DIR_INVERSE)
            {
                FillScaleOffset(
                    m_minOutValue, m_maxOutValue, m_minInValue, m_maxInValue, m_scale, m_offset);
            }
            else
            {
                FillScaleOffset(
                    m_minInValue, m_maxInValue, m_minOutValue, m_maxOutValue, m_scale, m_offset);
            }
        }

        // Create an identity range
        RangeOp::RangeOp(TransformDirection direction)
            :   Op()
            ,   m_minInValue(RangeUtils::GetNoLimitValue())
            ,   m_maxInValue(RangeUtils::GetNoLimitValue())
            ,   m_minOutValue(RangeUtils::GetNoLimitValue())
            ,   m_maxOutValue(RangeUtils::GetNoLimitValue())
            ,   m_direction(direction)
            ,   m_scale(1.0f)
            ,   m_offset(0.0f)
        {
            if(m_direction == TRANSFORM_DIR_UNKNOWN)
            {
                throw Exception("Cannot apply RangeOp op, unspecified transform direction.");
            }
        }

        void RangeOp::FillScaleOffset(
            float minInValue, float maxInValue, float minOutValue, float maxOutValue, 
            float & scale, float & offset)
        {
            scale = 1.0f;

            if(RangeUtils::IsNoLimitValue(minInValue))
            {
                if(RangeUtils::IsNoLimitValue(maxInValue))
                {
                    offset = 0.f;
                }
                else  // Bottom unlimited but top clamps
                {
                    offset = maxOutValue - scale * maxInValue;
                }
            }
            else
            {
                if(RangeUtils::IsNoLimitValue(maxInValue))  // Top unlimited but bottom clamps
                {
                    offset = minOutValue - scale * minInValue;
                }
                else  // Both ends clamp
                {
                    const double denom = maxInValue - minInValue;
                    if(fabs(denom) < 1e-6)
                    {
                        throw Exception("Range - Limits are too close");
                    }

                    // NB: Allowing out min == max as it could be useful to create a constant.
                    scale = (float)((maxOutValue - minOutValue) / denom);
                    offset = minOutValue - scale * minInValue;
                }
            }
        }

        void RangeOp::validate()
        {
            // If in_min or out_min is not empty, so must the other half be.
            if(RangeUtils::IsNoLimitValue(m_minInValue))
            {
                if(!RangeUtils::IsNoLimitValue(m_minOutValue))
                {
                    throw Exception("Range - In and out minimum limits must be both set or both missing");
                }
            }
            else
            {
                if (RangeUtils::IsNoLimitValue(m_minOutValue) )
                {
                    throw Exception("Range - In and out minimum limits must be both set or both missing");
                }
            }

            if (RangeUtils::IsNoLimitValue(m_maxInValue ) )
            {
                if(!RangeUtils::IsNoLimitValue(m_maxOutValue) )
                {
                    throw Exception("Range - In and out maximum limits must be both set or both missing");
                }
            }
            else
            {
                if(RangeUtils::IsNoLimitValue(m_maxOutValue) )
                {
                    throw Exception("Range - In and out maximum limits must be both set or both missing");
                }
            }

            // Currently not allowing polarity inversion so enforce max > min.
            if(!RangeUtils::IsNoLimitValue(m_minInValue) && !RangeUtils::IsNoLimitValue(m_maxInValue))
            {
                if(m_minInValue > m_maxInValue)
                {
                    throw Exception("Range - Maximumn input value is less than minimum input value");
                }
                if(m_minOutValue > m_maxOutValue)
                {
                    throw Exception("Range - Maximumn output value is less than minimum output value");
                }
            }
        }

        OpRcPtr RangeOp::clone() const
        {
            return 
                OpRcPtr(new RangeOp(
                    m_minInValue, m_maxInValue, m_minOutValue, m_maxOutValue, m_direction));
        }
        
        RangeOp::~RangeOp()
        {
        }
        
        std::string RangeOp::getInfo() const
        {
            return "<RangeOp>";
        }
        
        std::string RangeOp::getCacheID() const
        {
            return m_cacheID;
        }
        
        bool RangeOp::isNoOp() const
        {
            const bool hasInputLimts
                = m_direction==TRANSFORM_DIR_FORWARD
                    ?   (!RangeUtils::IsNoLimitValue(m_minInValue) 
                            || !RangeUtils::IsNoLimitValue(m_maxInValue))
                    :   (!RangeUtils::IsNoLimitValue(m_minOutValue) 
                            || !RangeUtils::IsNoLimitValue(m_maxOutValue));

            if(hasInputLimts)
            {
                return false;
            }

            return !scale();
        }

        bool RangeOp::scale() const
        {
            return m_scale!=1.0f || m_offset!=0.0f;
        }
        
        bool RangeOp::isSameType(const OpRcPtr & op) const
        {
            RangeOpRcPtr typedRcPtr = DynamicPtrCast<RangeOp>(op);
            return (bool)typedRcPtr;
        }
        
        bool RangeOp::isInverse(const OpRcPtr & op) const
        {
            RangeOpRcPtr typedRcPtr = DynamicPtrCast<RangeOp>(op);
            if(!typedRcPtr) return false;
            
            if(GetInverseTransformDirection(m_direction) != typedRcPtr->m_direction)
            {
                return false;
            }
            
            static const float error = 1e-7f;

            return RangeUtils::AreEqualValues(m_minInValue,  typedRcPtr->m_minInValue,  error)
                && RangeUtils::AreEqualValues(m_maxInValue,  typedRcPtr->m_maxInValue,  error)
                && RangeUtils::AreEqualValues(m_minOutValue, typedRcPtr->m_minOutValue, error)
                && RangeUtils::AreEqualValues(m_maxOutValue, typedRcPtr->m_maxOutValue, error);
        }
        
        bool RangeOp::canCombineWith(const OpRcPtr & op) const
        {
            return isSameType(op);
        }
        
        void RangeOp::combineWith(OpRcPtrVec & ops, const OpRcPtr & secondOp) const
        {
            RangeOpRcPtr typedRcPtr = DynamicPtrCast<RangeOp>(secondOp);
            if(!typedRcPtr)
            {
                std::ostringstream os;
                os << "RangeOp can only be combined with other ";
                os << "RangeOps.  secondOp:" << secondOp->getInfo();
                throw Exception(os.str().c_str());
            }

            float minInValue, maxInValue, minOutValue, maxOutValue;

            if(m_direction==TRANSFORM_DIR_FORWARD)
            {
                minInValue = m_minInValue;
                maxInValue = m_maxInValue;
                if(typedRcPtr->m_direction==TRANSFORM_DIR_FORWARD)
                {
                    minOutValue = typedRcPtr->m_minOutValue;
                    maxOutValue = typedRcPtr->m_maxOutValue;
                }
                else
                {
                    minOutValue = typedRcPtr->m_minInValue;
                    maxOutValue = typedRcPtr->m_maxInValue;
                }
            }
            else
            {
                minInValue = m_minOutValue;
                maxInValue = m_maxOutValue;
                if(typedRcPtr->m_direction==TRANSFORM_DIR_FORWARD)
                {
                    minOutValue = typedRcPtr->m_minOutValue;
                    maxOutValue = typedRcPtr->m_maxOutValue;
                }
                else
                {
                    minOutValue = typedRcPtr->m_minInValue;
                    maxOutValue = typedRcPtr->m_maxInValue;
                }
            }

            CreateRangeOp(
                ops, minInValue, maxInValue, minOutValue, maxOutValue, TRANSFORM_DIR_FORWARD);
        }

        bool RangeOp::hasChannelCrosstalk() const
        {
            return false;
        }
        
        void RangeOp::finalize()
        {
            // Validate the values
            //
            validate();

            // Rebuild the scale & offset values
            //
            if(m_direction == TRANSFORM_DIR_INVERSE)
            {
                FillScaleOffset(
                    m_minOutValue, m_maxOutValue, m_minInValue, m_maxInValue, m_scale, m_offset);
            }
            else
            {
                FillScaleOffset(
                    m_minInValue, m_maxInValue, m_minOutValue, m_maxOutValue, m_scale, m_offset);
            }

            // Rebuild the cache identifier
            //
            const float limits[4] = { m_minInValue, m_maxInValue, m_minOutValue, m_maxOutValue };

            // Create the cacheID
            md5_state_t state;
            md5_byte_t digest[16];
            md5_init(&state);
            md5_append(&state, (const md5_byte_t *)&limits[0], (int)(4*sizeof(float)));
            md5_finish(&state, digest);
            
            std::ostringstream cacheIDStream;
            cacheIDStream << "<RangeOp ";
            cacheIDStream << GetPrintableHash(digest) << " ";
            cacheIDStream << TransformDirectionToString(m_direction) << " ";
            cacheIDStream << ">";
            
            m_cacheID = cacheIDStream.str();
        }

        void RangeOp::apply(float * rgbaBuffer, long numPixels) const
        {
            float outputMin = m_direction==TRANSFORM_DIR_FORWARD ? m_minOutValue : m_minInValue;
            float outputMax = m_direction==TRANSFORM_DIR_FORWARD ? m_maxOutValue : m_maxInValue;

            if(RangeUtils::IsNoLimitValue(outputMin))
            {
                outputMin = -std::numeric_limits<float>::max();
            }

            if(RangeUtils::IsNoLimitValue(outputMax))
            {
                outputMax = std::numeric_limits<float>::max();
            }

#ifdef USE_SSE
            __m128 scaleValues  = _mm_set_ps(1.0f, m_scale, m_scale, m_scale);
            __m128 offsetValues = _mm_set_ps(0.0f, m_offset, m_offset, m_offset);

            __m128 minValues = _mm_set_ps(-std::numeric_limits<float>::max(), outputMin, outputMin, outputMin);
            __m128 maxValues = _mm_set_ps(std::numeric_limits<float>::max(), outputMax, outputMax, outputMax);
#endif

            for(long idx=0; idx<numPixels; ++idx)
            {
#ifdef USE_SSE
                __m128 pixel 
                    = _mm_set_ps(rgbaBuffer[3], rgbaBuffer[2], rgbaBuffer[1], rgbaBuffer[0]);

                pixel = _mm_add_ps(_mm_mul_ps(pixel, scaleValues), offsetValues);
                pixel = _mm_min_ps(_mm_max_ps(pixel, minValues), maxValues);

                _mm_storeu_ps(rgbaBuffer, pixel);

#else
                rgbaBuffer[dep] 
                    = std::min(std::max(rgbaBuffer[0] * m_scale + m_offset, outputMin), outputMax);
                rgbaBuffer[dep+1] 
                    = std::min(std::max(rgbaBuffer[1] * m_scale + m_offset, outputMin), outputMax);
                rgbaBuffer[dep+2] 
                    = std::min(std::max(rgbaBuffer[2] * m_scale + m_offset, outputMin), outputMax);
                // No alpha processing
#endif
                rgbaBuffer += 4;
            }
        }

        bool RangeOp::supportsGpuShader() const
        {
            return true;
        }

        void RangeOp::writeGpuShader(std::ostream & shader,
                                     const std::string & pixelName,
                                     const GpuShaderDesc & shaderDesc) const
        {
            const GpuLanguage lang = shaderDesc.getLanguage();

            if(scale())
            {
                const float scaleValues[4]  = { m_scale,  m_scale,  m_scale,  1.0f };
                const float offsetValues[4] = { m_offset, m_offset, m_offset, 0.0f };

                shader << pixelName << " = " << pixelName << " * ";
                Write_half4(shader, scaleValues, lang);
                shader << " + ";
                Write_half4(shader, offsetValues, lang);
                shader << ";\n";
            }

            if(!RangeUtils::IsNoLimitValue(m_minOutValue))
            {
                shader << pixelName << ".rgb = " 
                    << "max(" << pixelName << ".rgb, " << m_minOutValue << ");\n";
            }

            if(!RangeUtils::IsNoLimitValue(m_maxOutValue))
            {
                shader << pixelName << ".rgb = " 
                    << "min(" << pixelName << ".rgb, " << m_maxOutValue << ");\n";
            }
        }

    }  // Anon namespace
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////
    
    
    
    
    void CreateRangeOp(OpRcPtrVec & ops,
                       float minInValue, float maxInValue, float minOutValue, float maxOutValue,
                       TransformDirection direction)
    {
        ops.push_back( 
            RangeOpRcPtr(new RangeOp(minInValue, maxInValue, minOutValue, maxOutValue, direction)) );
    }


    void CreateIdentityRangeOp(OpRcPtrVec & ops, TransformDirection direction)
    {
        ops.push_back( RangeOpRcPtr(new RangeOp(direction)) );
    }


    float RangeUtils::GetNoLimitValue()
    {
        return std::numeric_limits<float>::quiet_NaN();
    }


    bool RangeUtils::IsNoLimitValue(float value)
    {
        return F_ISNAN(value);
    }

    bool RangeUtils::AreEqualValues(float left, float right, float absoluteError)
    {
        if(IsNoLimitValue(left))
        {
            return IsNoLimitValue(right);
        }
        else
        {
            if(IsNoLimitValue(right))
            {
                return false;
            }
            else
            {
                return equalWithAbsError(left, right, absoluteError);
            }
        }
    }

}
OCIO_NAMESPACE_EXIT



////////////////////////////////////////////////////////////////////////////////


#ifdef OCIO_UNIT_TEST

namespace OCIO = OCIO_NAMESPACE;
#include "UnitTest.h"

OCIO_NAMESPACE_USING

const float error = 1e-7f;

OIIO_ADD_TEST(RangeOps, Identity)
{
    OCIO::OpRcPtrVec ops;
    OCIO::CreateIdentityRangeOp(ops, OCIO::TRANSFORM_DIR_FORWARD);
    OIIO_CHECK_EQUAL(ops.size(), 1);

    float image[4*3] = { -0.50f, -0.25f, 0.50f, 0.0f,
                          0.75f,  1.00f, 1.25f, 1.0f,
                          1.25f,  1.50f, 1.75f, 0.0f };

    ops[0]->finalize();
    ops[0]->apply(&image[0], 3);

    OIIO_CHECK_CLOSE(image[0],  -0.50f, error);
    OIIO_CHECK_CLOSE(image[1],  -0.25f, error);
    OIIO_CHECK_CLOSE(image[2],   0.50f, error);
    OIIO_CHECK_CLOSE(image[3],   0.00f, error);
    OIIO_CHECK_CLOSE(image[4],   0.75f, error);
    OIIO_CHECK_CLOSE(image[5],   1.00f, error);
    OIIO_CHECK_CLOSE(image[6],   1.25f, error);
    OIIO_CHECK_CLOSE(image[7],   1.00f, error);
    OIIO_CHECK_CLOSE(image[8],   1.25f, error);
    OIIO_CHECK_CLOSE(image[9],   1.50f, error);
    OIIO_CHECK_CLOSE(image[10],  1.75f, error);
    OIIO_CHECK_CLOSE(image[11],  0.00f, error);
}

OIIO_ADD_TEST(RangeOps, OffsetOnly)
{
    OCIO::OpRcPtrVec ops;
    OCIO::CreateRangeOp(ops, 0.0f, 1.0f, 0.5f, 1.5f, OCIO::TRANSFORM_DIR_FORWARD);
    OIIO_CHECK_EQUAL(ops.size(), 1);
    ops[0]->finalize();

    float image[4*3] = { -0.50f, -0.25f, 0.50f, 0.0f,
                          0.75f,  1.00f, 1.25f, 1.0f,
                          1.25f,  1.50f, 1.75f, 0.0f };

    ops[0]->apply(&image[0], 3);

    OIIO_CHECK_CLOSE(image[0],  0.50f, error);
    OIIO_CHECK_CLOSE(image[1],  0.50f, error);
    OIIO_CHECK_CLOSE(image[2],  1.00f, error);
    OIIO_CHECK_CLOSE(image[3],  0.00f, error);
    OIIO_CHECK_CLOSE(image[4],  1.25f, error);
    OIIO_CHECK_CLOSE(image[5],  1.50f, error);
    OIIO_CHECK_CLOSE(image[6],  1.50f, error);
    OIIO_CHECK_CLOSE(image[7],  1.00f, error);
    OIIO_CHECK_CLOSE(image[8],  1.50f, error);
    OIIO_CHECK_CLOSE(image[9],  1.50f, error);
    OIIO_CHECK_CLOSE(image[10], 1.50f, error);
    OIIO_CHECK_CLOSE(image[11], 0.00f, error);
}

OIIO_ADD_TEST(RangeOps, ScaleOnly)
{
    OCIO::OpRcPtrVec ops;
    OCIO::CreateRangeOp(ops, 0.0f, 1.0f, 0.0f, 1.5f, OCIO::TRANSFORM_DIR_FORWARD);
    OIIO_CHECK_EQUAL(ops.size(), 1);
    ops[0]->finalize();

    float image[4*3] = { -0.50f, -0.25f, 0.50f, 0.0f,
                          0.75f,  1.00f, 1.25f, 1.0f,
                          1.25f,  1.50f, 1.75f, 0.0f };

    ops[0]->apply(&image[0], 3);

    OIIO_CHECK_CLOSE(image[0],  0.000f, error);
    OIIO_CHECK_CLOSE(image[1],  0.000f, error);
    OIIO_CHECK_CLOSE(image[2],  0.750f, error);
    OIIO_CHECK_CLOSE(image[3],  0.000f, error);
    OIIO_CHECK_CLOSE(image[4],  1.125f, error);
    OIIO_CHECK_CLOSE(image[5],  1.500f, error);
    OIIO_CHECK_CLOSE(image[6],  1.500f, error);
    OIIO_CHECK_CLOSE(image[7],  1.000f, error);
    OIIO_CHECK_CLOSE(image[8],  1.500f, error);
    OIIO_CHECK_CLOSE(image[9],  1.500f, error);
    OIIO_CHECK_CLOSE(image[10], 1.500f, error);
    OIIO_CHECK_CLOSE(image[11], 0.000f, error);
}

OIIO_ADD_TEST(RangeOps, Arbitrary)
{
    OCIO::OpRcPtrVec ops;
    OCIO::CreateRangeOp(ops, 0.0f, 1.0f, 0.0f, 1.0f, OCIO::TRANSFORM_DIR_FORWARD);
    OIIO_CHECK_EQUAL(ops.size(), 1);
    ops[0]->finalize();

    float image[4*3] = { -0.50f, -0.25f, 0.50f, 0.0f,
                          0.75f,  1.00f, 1.25f, 1.0f,
                          1.25f,  1.50f, 1.75f, 0.0f };

    ops[0]->apply(&image[0], 3);

    OIIO_CHECK_CLOSE(image[0],  0.00f, error);
    OIIO_CHECK_CLOSE(image[1],  0.00f, error);
    OIIO_CHECK_CLOSE(image[2],  0.50f, error);
    OIIO_CHECK_CLOSE(image[3],  0.00f, error);
    OIIO_CHECK_CLOSE(image[4],  0.75f, error);
    OIIO_CHECK_CLOSE(image[5],  1.00f, error);
    OIIO_CHECK_CLOSE(image[6],  1.00f, error);
    OIIO_CHECK_CLOSE(image[7],  1.00f, error);
    OIIO_CHECK_CLOSE(image[8],  1.00f, error);
    OIIO_CHECK_CLOSE(image[9],  1.00f, error);
    OIIO_CHECK_CLOSE(image[10], 1.00f, error);
    OIIO_CHECK_CLOSE(image[11], 0.00f, error);
}

OIIO_ADD_TEST(RangeOps, Combining)
{
    OCIO::OpRcPtrVec ops;

    OCIO::CreateRangeOp(ops, 0.0f, 0.5f, 0.5f, 1.0f, OCIO::TRANSFORM_DIR_FORWARD);
    OIIO_CHECK_EQUAL(ops.size(), 1);
    ops[0]->finalize();
    OCIO::CreateRangeOp(ops, 0.0f, 1.0f, 0.5f, 1.5f, OCIO::TRANSFORM_DIR_FORWARD);
    OIIO_CHECK_EQUAL(ops.size(), 2);
    ops[1]->finalize();

    ops[0]->combineWith(ops, ops[1]);
    OIIO_CHECK_EQUAL(ops.size(), 3);
    ops[2]->finalize();

    OCIO::RangeOpRcPtr typedRcPtr = OCIO::DynamicPtrCast<RangeOp>(ops[2]);
    OIIO_CHECK_EQUAL(typedRcPtr->getMinInValue(),  0.0f);
    OIIO_CHECK_EQUAL(typedRcPtr->getMaxInValue(),  0.5f);
    OIIO_CHECK_EQUAL(typedRcPtr->getMinOutValue(), 0.5f);
    OIIO_CHECK_EQUAL(typedRcPtr->getMaxOutValue(), 1.5f);
}

OIIO_ADD_TEST(RangeOps, CombiningInverse)
{
    OCIO::OpRcPtrVec ops;

    OCIO::CreateRangeOp(ops, 0.0f, 1.0f, 0.5f, 1.5f, OCIO::TRANSFORM_DIR_FORWARD);
    OIIO_CHECK_EQUAL(ops.size(), 1);
    ops[0]->finalize();
    OCIO::CreateRangeOp(ops, 0.0f, 1.0f, 0.5f, 1.5f, OCIO::TRANSFORM_DIR_INVERSE);
    OIIO_CHECK_EQUAL(ops.size(), 2);
    ops[1]->finalize();

    ops[0]->combineWith(ops, ops[1]);
    OIIO_CHECK_EQUAL(ops.size(), 3);
    ops[2]->finalize();

    OCIO::RangeOpRcPtr typedRcPtr = OCIO::DynamicPtrCast<RangeOp>(ops[2]);
    OIIO_CHECK_EQUAL(typedRcPtr->getMinInValue(),  0.0f);
    OIIO_CHECK_EQUAL(typedRcPtr->getMaxInValue(),  1.0f);
    OIIO_CHECK_EQUAL(typedRcPtr->getMinOutValue(), 0.0f);
    OIIO_CHECK_EQUAL(typedRcPtr->getMaxOutValue(), 1.0f);
}

OIIO_ADD_TEST( RangeOps, Apply )
{
  // NB: The Sx_LCx_HCx notation in the range test cases refers to whether
  // there is scaling, clipping on the low end, and clipping on the high end
  // with 1 meaning yes and 0 meaning no.
/*
  // complete RangeRender (S1_LC1_HC1)
  {
    uint8_t rgbaImage[8] = { 0, 128, 255,   0,
                            16, 235,  17, 128 };

    float resImage[8] = { 0, 0, 0, 0,
                          0, 0, 0, 0 };

    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_8i, SYNCOLOR::BIT_DEPTH_32f,
                      16., 235., -0.5, 2. );

    BOOST_CHECK(F_EQUAL(r1.getLowBound(),-0.5f));
    BOOST_CHECK(F_EQUAL(r1.getHighBound(),2.f));

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::RangeScaleMinMax);

    pRenderer->apply(rgbaImage, resImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);

    BOOST_CHECK(F_EQUAL(resImage[0],-0.5f));
    BOOST_CHECK(F_EQUAL(resImage[1],0.77853881f));
    BOOST_CHECK(F_EQUAL(resImage[2],2.0f));
    BOOST_CHECK(F_EQUAL(resImage[3],0.f));
    BOOST_CHECK(F_EQUAL(resImage[4],-0.5f));
    BOOST_CHECK(F_EQUAL(resImage[5],2.0f));
    BOOST_CHECK(F_EQUAL(resImage[6],-0.48858447f));
    BOOST_CHECK(F_EQUAL(resImage[7],128.f/255.f));
  }

  // S1_LC1_HC0
  {
    uint8_t rgbaImage[8] = { 0, 128, 255,   0,
                            16, 235,  17, 128 };

    float resImage[8] = { 0, 0, 0, 0,
                          0, 0, 0, 0 };

    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_8i, SYNCOLOR::BIT_DEPTH_32f,
                      16., emptyValue, -0.5, emptyValue );

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::RangeScaleMin);

    pRenderer->apply(rgbaImage, resImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);


    // \todo Each of these tests should check that the correct algorithm
    //    gets called.
    //    e.g. something like:
    //       BOOST_CHECK(renderer->getOp().getTypeName()=="S1_LC1_HC0");

    BOOST_CHECK(F_EQUAL(resImage[0],-0.5f));
    BOOST_CHECK(F_EQUAL(resImage[1],-0.06078428f));
    BOOST_CHECK(F_EQUAL(resImage[2],0.43725491f));
    BOOST_CHECK(F_EQUAL(resImage[3],0.f));
    BOOST_CHECK(F_EQUAL(resImage[4],-0.5f));
    BOOST_CHECK(F_EQUAL(resImage[5],0.3588236f));
    BOOST_CHECK(F_EQUAL(resImage[6],-0.49607843f));
    BOOST_CHECK(F_EQUAL(resImage[7],128.f/255.f));
  }

  // S1_LC0_HC1
  {
    uint8_t rgbaImage[8] = { 0, 128, 255,   0,
                            16, 235,  17, 128 };

    float resImage[8] = { 0, 0, 0, 0,
                          0, 0, 0, 0 };

    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_8i, SYNCOLOR::BIT_DEPTH_32f,
                      emptyValue, 235., emptyValue, 1.2 );

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::RangeScaleMax);

    pRenderer->apply(rgbaImage, resImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);

    BOOST_CHECK(F_EQUAL(resImage[0],0.27843139f));
    BOOST_CHECK(F_EQUAL(resImage[1],0.78039217f));
    BOOST_CHECK(F_EQUAL(resImage[2],1.2f));
    BOOST_CHECK(F_EQUAL(resImage[3],0.f));
    BOOST_CHECK(F_EQUAL(resImage[4],0.34117648f));
    BOOST_CHECK(F_EQUAL(resImage[5],1.2f));
    BOOST_CHECK(F_EQUAL(resImage[6],0.34509805f));
    BOOST_CHECK(F_EQUAL(resImage[7],128.f/255.f));
  }

  // S1_LC0_HC0
  {
    uint8_t rgbaImage[8] = { 0, 128, 255,   0,
                            16, 235,  17, 128 };

    half resImage[8] = { 0, 0, 0, 0,
                         0, 0, 0, 0 };

    // NB: Scale required for bit-depth conversion.
    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_8i, SYNCOLOR::BIT_DEPTH_16f,
                      emptyValue, emptyValue, emptyValue, emptyValue );

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::RangeScale);

    pRenderer->apply(rgbaImage, resImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);

    BOOST_CHECK(H_EQUAL(resImage[0],0.f));
    BOOST_CHECK(H_EQUAL(resImage[1],128.f/255.f));
    BOOST_CHECK(H_EQUAL(resImage[2],1.f));
    BOOST_CHECK(H_EQUAL(resImage[3],0.f));
    BOOST_CHECK(H_EQUAL(resImage[4],16.f/255.f));
    BOOST_CHECK(H_EQUAL(resImage[5],235.f/255.f));
    BOOST_CHECK(H_EQUAL(resImage[6],17.f/255.f));
    BOOST_CHECK(H_EQUAL(resImage[7],128.f/255.f));
  }

  // S0_LC1_HC1
  {
    uint8_t rgbaImage[8] = { 0, 128, 255,   0,
                            16, 235,  17, 128 };

    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_8i, SYNCOLOR::BIT_DEPTH_8i,
                      16., 235., 16., 235. );

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::RangeMinMax);

    pRenderer->apply(rgbaImage, rgbaImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);

    BOOST_CHECK(rgbaImage[0]==16);
    BOOST_CHECK(rgbaImage[1]==128);
    BOOST_CHECK(rgbaImage[2]==235);
    BOOST_CHECK(rgbaImage[3]==0);
    BOOST_CHECK(rgbaImage[4]==16);
    BOOST_CHECK(rgbaImage[5]==235);
    BOOST_CHECK(rgbaImage[6]==17);
    BOOST_CHECK(rgbaImage[7]==128);
  }

  // \todo Test NaN.

  // S0_LC1_HC0
  {
    float rgbaImage[8] = { -0.1f, 0.0f, 0.5f, -0.1f,
                            1.0f, 1.2f, 10.0f, 0.5f };

    // Clip negs but not > 1.
    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_32f, SYNCOLOR::BIT_DEPTH_32f,
                      0., emptyValue, 0., emptyValue );

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::RangeMin);

    pRenderer->apply(rgbaImage, rgbaImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);

    BOOST_CHECK(F_EQUAL(rgbaImage[0],0.f));
    BOOST_CHECK(F_EQUAL(rgbaImage[1],0.f));
    BOOST_CHECK(F_EQUAL(rgbaImage[2],0.5f));
    BOOST_CHECK(F_EQUAL(rgbaImage[3],-0.1f));  // alpha not clipped
    BOOST_CHECK(F_EQUAL(rgbaImage[4],1.0f));
    BOOST_CHECK(F_EQUAL(rgbaImage[5],1.2f));
    BOOST_CHECK(F_EQUAL(rgbaImage[6],10.f));
    BOOST_CHECK(F_EQUAL(rgbaImage[7],0.5f));
  }

  // S0_LC0_HC1
  {
    float rgbaImage[8] = { -0.1f, 0.0f, 0.5f, 1.2f,
                            1.0f, 1.2f, 10.0f, 0.5f };

    half resImage[8] = { 0, 0, 0, 0,
                         0, 0, 0, 0 };

    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_32f, SYNCOLOR::BIT_DEPTH_16f,
                      emptyValue, 5., emptyValue, 5. );

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::RangeMax);

    pRenderer->apply(rgbaImage, resImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);

    BOOST_CHECK(H_EQUAL(resImage[0],-0.1f));
    BOOST_CHECK(H_EQUAL(resImage[1],0.f));
    BOOST_CHECK(H_EQUAL(resImage[2],0.5f));
    BOOST_CHECK(H_EQUAL(resImage[3],1.2f));  // alpha not clipped
    BOOST_CHECK(H_EQUAL(resImage[4],1.0f));
    BOOST_CHECK(H_EQUAL(resImage[5],1.2f));
    BOOST_CHECK(H_EQUAL(resImage[6],5.f));
    BOOST_CHECK(H_EQUAL(resImage[7],0.5f));
  }

  // S0_LC0_HC0
  {
    uint8_t rgbaImage[8] = { 0, 128, 255,   0,
                            16, 235,  17, 128 };

    // Clipping requested but not necessary.  This is a no-op.
    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_8i, SYNCOLOR::BIT_DEPTH_8i,
                      -16., 275., -16., 275. );

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    // Note: CPURangeOp.cpp returns a null virtual renderer in this case.
    // This results in an empty processor and CPURenderer.cpp:createProcessor
    // adds a matrix to copy the pixels.
    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::MatrixScale);

    pRenderer->apply(rgbaImage, rgbaImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);

    BOOST_CHECK(rgbaImage[0]==0);
    BOOST_CHECK(rgbaImage[1]==128);
    BOOST_CHECK(rgbaImage[2]==255);
    BOOST_CHECK(rgbaImage[3]==0);
    BOOST_CHECK(rgbaImage[4]==16);
    BOOST_CHECK(rgbaImage[5]==235);
    BOOST_CHECK(rgbaImage[6]==17);
    BOOST_CHECK(rgbaImage[7]==128);
  }

  // Out max == out min
  // Should be S1_LC0_HC0
  {
    uint8_t rgbaImage[8] = { 0, 128, 255,   0,
                            16, 235,  17, 128 };

    float resImage[8] = { 0, 0, 0, 0,
                          0, 0, 0, 0 };

    Color::RangeOp r1(SYNCOLOR::BIT_DEPTH_8i, SYNCOLOR::BIT_DEPTH_32f,
                      16., 235., 0.2, 0.2 );

    Color::TransformPtr p(new Color::Transform());
    p->getOps().append(r1.clone(Color::Op::DO_SHALLOW_COPY));

    const SYNCOLOR::BitDepth inBD = p->getInputBitDepth();
    const SYNCOLOR::BitDepth outBD = p->getOutputBitDepth();

    pRenderer->finalize(
        p,
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|inBD),
        (SYNCOLOR::PixelFormat)(Color::Utils::PXL_LAYOUT_RGBA|outBD));

    BOOST_REQUIRE_EQUAL(pRenderer->getRendererType(0), Rendering::RangeScale);

    pRenderer->apply(rgbaImage, resImage,
                     SYNCOLOR::ROI(2, 1),
                     0, 0);

    BOOST_CHECK(F_EQUAL(resImage[0],0.2f));
    BOOST_CHECK(F_EQUAL(resImage[1],0.2f));
    BOOST_CHECK(F_EQUAL(resImage[2],0.2f));
    BOOST_CHECK(F_EQUAL(resImage[3],0.f));
    BOOST_CHECK(F_EQUAL(resImage[4],0.2f));
    BOOST_CHECK(F_EQUAL(resImage[5],0.2f));
    BOOST_CHECK(F_EQUAL(resImage[6],0.2f));
    BOOST_CHECK(F_EQUAL(resImage[7],128.f/255.f));
  }
*/
}

#endif
