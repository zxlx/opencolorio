/*
    Made by Autodesk Inc. under the terms of the OpenColorIO BSD 3 Clause License
*/


#include <OpenColorIO/OpenColorIO.h>


namespace OCIO = OCIO_NAMESPACE;
#include "GPUUnitTest.h"
#include "GPUHelpers.h"

#include <stdio.h>
#include <sstream>
#include <string>

OCIO_NAMESPACE_USING


const float epsilon = 1e-7f;


OCIO_ADD_GPU_TEST(RangeOp, Identity)
{
    OCIO::RangeTransformRcPtr range = OCIO::RangeTransform::Create();
    test.setContext(range->createEditableCopy(), epsilon);
}

OCIO_ADD_GPU_TEST(RangeOp, OffsetOnly)
{
    OCIO::RangeTransformRcPtr range = OCIO::RangeTransform::Create();
    range->setMinInValue(0.0f);
    range->setMaxInValue(1.0f);
    range->setMinOutValue(0.5f);
    range->setMaxOutValue(1.5f);

    test.setContext(range->createEditableCopy(), epsilon);
}

OCIO_ADD_GPU_TEST(RangeOp, ScaleOnly)
{
    OCIO::RangeTransformRcPtr range = OCIO::RangeTransform::Create();
    range->setMinInValue(0.0f);
    range->setMaxInValue(1.0f);
    range->setMinOutValue(0.0f);
    range->setMaxOutValue(1.5f);

    test.setContext(range->createEditableCopy(), epsilon);
}

OCIO_ADD_GPU_TEST(RangeOp, Arbitrary_1)
{
    OCIO::RangeTransformRcPtr range = OCIO::RangeTransform::Create();
    range->setMinInValue(0.4f);
    range->setMaxInValue(0.6f);
    range->setMinOutValue(0.4f);
    range->setMaxOutValue(0.6f);

    test.setContext(range->createEditableCopy(), epsilon);
}

OCIO_ADD_GPU_TEST(RangeOp, Arbitrary_2)
{
    OCIO::RangeTransformRcPtr range = OCIO::RangeTransform::Create();
    range->setMinInValue(-0.4f);
    range->setMaxInValue(0.6f);
    range->setMinOutValue(0.2f);
    range->setMaxOutValue(1.6f);

    test.setContext(range->createEditableCopy(), 1e-6f);
}
