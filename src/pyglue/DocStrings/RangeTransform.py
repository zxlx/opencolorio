"""
    Made by Autodesk Inc. under the terms of the OpenColorIO BSD 3 Clause License
"""


class RangeTransform:
    """
    RangeTransform
    """
    def __init__(self):
        pass
    
    def getMinInValue(self):
        """
        getMinInValue()
        
        Returns the minimum input value of :py:class:`PyOpenColorIO.RangeTransform`.
        """
        pass

    def setMinInValue(self, value):
        """
        setMinInValue(value)
        
        Sets the minimum input value in :py:class:`PyOpenColorIO.RangeTransform`.
        
        :param value: minimum input value of range transform
        :type value: float
        """
        pass

    def hasMinInValue(self):
        """
        hasMinInValue()
        
        Returns true if the minimum input value of :py:class:`PyOpenColorIO.RangeTransform` is set.
        """
        pass

    def getMaxInValue(self):
        """
        getMaxInValue()
        
        Returns the maximum input value of :py:class:`PyOpenColorIO.RangeTransform`.
        """
        pass
    
    def setMaxInValue(self, value):
        """
        setMaxInValue(value)
        
        Sets the maximum input value in :py:class:`PyOpenColorIO.RangeTransform`.
        
        :param value: maximum input alue of range transform
        :type value: float
        """
        pass

    def hasMaxInValue(self):
        """
        hasMaxInValue()
        
        Returns true if the maximum input value of :py:class:`PyOpenColorIO.RangeTransform` is set.
        """
        pass

    def getMinOutValue(self):
        """
        getMinOutValue()
        
        Returns the minimum output value of :py:class:`PyOpenColorIO.RangeTransform`.
        """
        pass
    
    def setMinOutValue(self, value):
        """
        setMinOutValue(value)
        
        Sets the minimum output value in :py:class:`PyOpenColorIO.RangeTransform`.
        
        :param value: minimum output value of range transform
        :type value: float
        """
        pass

    def hasMinOutValue(self):
        """
        hasMinOutValue()
        
        Returns true if the minimum output value of :py:class:`PyOpenColorIO.RangeTransform` is set.
        """
        pass

    def getMaxOutValue(self):
        """
        getMaxOutValue()
        
        Returns the maximum output value of :py:class:`PyOpenColorIO.RangeTransform`.
        """
        pass
    
    def setMaxOutValue(self, value):
        """
        setMaxOutValue(value)
        
        Sets the maximum output value in :py:class:`PyOpenColorIO.RangeTransform`.
        
        :param value: maximum output value of range transform
        :type value: float
        """
        pass

    def hasMaxOutValue(self):
        """
        hasMaxOutValue()
        
        Returns true if the maximum output value of :py:class:`PyOpenColorIO.RangeTransform` is set.
        """
        pass
